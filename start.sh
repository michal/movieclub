poetry run ./manage.py migrate
poetry run ./manage.py collectstatic -c --no-input
poetry run gunicorn movieclub.wsgi -b 0.0.0.0:8000
