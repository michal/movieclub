from django.contrib import admin

from . import models

class MovieVoteInline(admin.StackedInline):
    model = models.MovieVote
    extra = 0

class MovieAdmin(admin.ModelAdmin):
    fields = [
        "name", "watched", "suggested_by", "score"
    ]
    readonly_fields = ("score",)
    list_display = ["name", "watched", "suggested_by", "score"]
    inlines = [
        MovieVoteInline
    ]

    @admin.display(description="Score")
    def score(self, instance):
        return instance.score


admin.site.register(models.Movie, MovieAdmin)
