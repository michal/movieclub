from django.test import TestCase

from watchlist.models import IMDB_ID_RE, CSFD_ID_RE

class IMDBIDTest(TestCase):

    def test_valid_imdb_id(self):
        valid = [
            "tt0000000",
            "tt1234567",
            "tt4975722",
            "tt3581920"
        ]
        for testId in valid:
            with self.subTest(testId=testId):
                self.assertTrue(IMDB_ID_RE.match(testId))
    
    def test_invalid_imdb_id(self):
        invalid = [
            "tt",
            "tt1",
            "1234567"
            "tt123456",
            "-tt1234567",
            ""
        ]
        for testId in invalid:
            with self.subTest(testId=testId):
                self.assertFalse(IMDB_ID_RE.match(testId))

    def test_url_extraction(self):
        urls = [
            ("https://www.imdb.com/title/tt4925292/?ref_=hm_rvi_tt_i_1", "tt4925292"),
            ("https://www.imdb.com/title/tt5580390/", "tt5580390"),
        ]
        for url, result in urls:
            with self.subTest(url=url, result=result):
                m = IMDB_ID_RE.search(url)
                self.assertTrue(m)
                self.assertEqual(m.group(), result)

class CSFDIDTest(TestCase):

    def test_valid_csfd_id(self):
        valid = [
            "1",
            "123",
            "969361"

        ]
        for testId in valid:
            with self.subTest(testId=testId):
                self.assertTrue(CSFD_ID_RE.match(testId))
    
    def test_invalid_csfd_id(self):
        invalid = [
            "abc",
            "-1",
            "0",
            "tt0000000",
            "",
        ]
        for testId in invalid:
            with self.subTest(testId=testId):
                self.assertFalse(CSFD_ID_RE.match(testId))

    def test_url_extraction(self):
        urls = [
            ("https://www.csfd.cz/film/1-predevsim-nikomu-neublizim/recenze/", "1"),
            ("https://www.csfd.cz/film/969361-velryba/prehled/", "969361"),
            ("https://www.csfd.cz/film/370706-daredevil/galerie/?page=20", "370706"),
            ("https://www.csfd.cz/film/370706", "370706")
        ]
        for url, result in urls:
            with self.subTest(url=url, result=result):
                m = CSFD_ID_RE.search(url)
                self.assertTrue(m)
                self.assertEqual(m.group(), result)
