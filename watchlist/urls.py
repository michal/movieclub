from django.urls import path
from . import views

app_name = "watchlist"
urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('movie/<int:pk>', views.DetailView.as_view(), name="detail"),
    path('movie/<int:pk>/vote', views.vote, name="vote"),
    path('movie/<int:pk>/edit', views.EditView.as_view(), name="edit"),
    path('movie/<int:pk>/delete', views.delete, name="delete"),
    path('movie', views.submit, name="submit")
]