import re

from functools import reduce
from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin

IMDB_ID_RE = re.compile(r'(?P<id>tt\d{7,})')
CSFD_ID_RE = re.compile(r'(?P<id>[1-9]\d*)')

def validate_imdb_id(v: str) -> bool:
    return IMDB_ID_RE.match(v)

def validate_csfd_id(v: str) -> bool:
    return CSFD_ID_RE.match(v)

class Movie(models.Model):

    class Meta:
        permissions = [
            ("moderate_movies", "Can edit other's movies and mark them as watched"),
        ]

    name = models.CharField(max_length=100)
    suggested_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    watched = models.BooleanField(default=False)
    imdb_id = models.CharField(max_length=32, blank=True, validators=[validate_csfd_id])
    csfd_id = models.CharField(max_length=32, blank=True, validators=[validate_imdb_id])

    @property
    def score(self):
        return reduce(lambda result,v: result+v.vote, self.movievote_set.filter(user__is_active=True).all(), 0)

    @property
    def seen_score(self):
        return reduce(lambda result,v: result+int(v.seen), self.movievote_set.filter(user__is_active=True).all(), 0)

    def __str__(self):
        return self.name

class MovieVote(models.Model):

    class Vote(models.IntegerChoices):
        UPVOTE = 1
        NOVOTE = 0
        DOWNVOTE = -1
    
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vote = models.IntegerField(choices=Vote.choices, default=Vote.NOVOTE)
    seen = models.BooleanField(default=False, null=True)
    comment = models.TextField(blank=True)

    def __str__(self):
        return f"{self.user.username}'s vote for {self.movie.name}"

