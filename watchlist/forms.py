from django import forms

from . import models

class MovieEditForm(forms.ModelForm):
    
    class Meta:
        model = models.Movie
        fields = ["name", "suggested_by", "watched", "imdb_id", "csfd_id"]