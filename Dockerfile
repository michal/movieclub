FROM python:3.11
EXPOSE 8000
WORKDIR /app

COPY poetry.lock pyproject.toml ./
RUN apt update && apt install -y -qq \
      python3-dev \
      default-libmysqlclient-dev \
      build-essential \
      && \
    pip3 install \
      poetry==1.3.0 \
      && \
    poetry add mysqlclient && \
    poetry install

COPY . /app
CMD ["bash", "/app/start.sh" ]

