from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.views import generic

from .forms import LoginForm

class LoginView(generic.TemplateView):
    template_name = "auth/login.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = LoginForm()
        if "next" in self.request.GET:
            context["next"] = self.request.GET["next"]
        return context

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(request.GET.get("next","/"))
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)
    
    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(request.POST.get("next","/"))
        form = LoginForm(request.POST)
        if not form.is_valid():
            return HttpResponseRedirect(reverse("auth:login"))
        user = authenticate(request, username=form.cleaned_data["username"], password=form.cleaned_data["password"])
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(request.GET.get("next", "/"))
        else:
            return HttpResponseRedirect(reverse("auth:login"))

class LogoutView(generic.RedirectView):

    def get_redirect_url(self):
        return self.request.GET.get("next", "/")

    def get(self, request, *args, **kwargs):
        logout(request)
        return super().get(request, *args, **kwargs)

def logout_view(request):

    logout(request)
    redirect_to = request.GET.get("redirect_to", "/")
    return HttpResponseRedirect(redirect_to)